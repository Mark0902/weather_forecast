package org.mark.weather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.mark.weather.weather.FullWeather;
import org.mark.weather.weather.Weather;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //?
        textView=findViewById(R.id.field);

        currentWeatherPrint();

    }

    public void currentWeatherPrint(){
        textView.setText("");
        WeatherApiInterface nsk= Controller.getApi();
        Call<FullWeather> call=nsk.getWeatherByIdWithLang(1496747,"f9ace13c28962cb2a2cc302161737fe8","ru","metric");
        call.enqueue(new Callback<FullWeather>() {
            @Override
            public void onResponse(Call<FullWeather> tCall,Response<FullWeather> response) {
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    FullWeather result = response.body();
                    textView.append(result.getName()+"\n");
                    textView.append("Скорость ветра : "+result.getWind().getSpeed()+" м/с\n");
                    double tempCels=result.getMain().getTemp();
                    tempCels = new BigDecimal(tempCels).setScale(2, RoundingMode.UP).doubleValue();
                    textView.append("Температура: "+Double.toString(tempCels)+" C'"+"\n");
                    List<Weather> list=result.getWeather();
                    textView.append("Состояние: "+list.get(0).getMain()+"\n");
                    textView.append("Давление: "+result.getMain().getPressure()+" pH\n");
                    textView.append("Влажность: "+result.getMain().getHumidity()+"% \n");
                    textView.append("Облачность: "+result.getClouds().getAll()+"% \n");


                } else {
                    //request not successful (like 400,401,403 etc)
                    //Handle errors
                    textView.append("Плохой реквест ");
                }
            }
            @Override
            public void onFailure(Call<FullWeather> tCall,Throwable t) {
                textView.append("Ошибка");

            }
        });

    }
    public void onClick(View view){
        currentWeatherPrint();
    }

}
