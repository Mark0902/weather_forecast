package org.mark.weather;

import org.mark.weather.weather.FullWeather;

import retrofit2.http.GET;
import retrofit2.Call;
import retrofit2.http.Query;

public interface WeatherApiInterface {

    @GET("weather")
    Call<FullWeather> getWeatherById(@Query("id") Integer id, @Query("APPID") String APPID);


    @GET("weather")
    Call<FullWeather> getWeatherByIdWithLang(@Query("id") Integer id, @Query("APPID") String APPID,@Query("lang") String lang,@Query("units")String units);

    @GET("/weather")
    Call<FullWeather> getWeatherForNSK();

}
